# Desafio Backend Developer - Solução André Fernandes Hachich

##### Instruções para execução do programa ###
Para executar o programa, basta ter instalado um compilador de Python 3.x e executar o arquivo main.py, localizado no root do diretório

A partir daí, basta seguir as instruções do prompt de comando. Os dados do programa são armazenados nos arquivos de texto da pasta ./data/ para garantir a persistência dos dados, e portanto a alteração desses arquivos manualmente pode causar um funcionamento não esperado.

O programa não está todo a prova de "usuários hostis". Embora muitos casos de erro tenham sido tratados, admite-se que há falhas, principalmente se o usuário inserir números negativos nos inputs de quantidades e valores. Ressalta-se que, com tempo suficiente, o desenvolvedor trataria todos esses casos.


##### Observações sobre o enunciado###
Por "desconto progressivo", entendeu-se que a cada item adicionado, todos os itens do mesmo fator teriam o mesmo desconto. Desta maneira, por exemplo:

 
 - Se o usuário colocou um produto do fator A no carrinho, ele recebeu 1% de desconto nesse produto.

 - Se o usuário colocou dois produtos do fator A no carrinho, ele recebeu 2% de desconto em ambos os produtos.

 - Se o usuário colocou três unidades do mesmo produto de fator A no carrinho, ele recebe 3% de desconto.
 
 
Notou-se, ainda, que a condição "o total de desconto do carrinho de compras não pode superar 30%" é matematicamente impossível de ser atingida. Como não há em nenhuma circunstância (em nenhum dos fatores) a possibilidade de um desconto maior que 30%, o desconto total não será no máximo igual a 30%.


##### Observações sobre a execução###
Por falta de tempo ou experiência, não foi possível completar todas as tarefas propostas. Procurou-se focar no essencial do programa, ou seja, o tratamento do catálogo de produtos da loja e dos carrinhos de compras dos usuários.

O que ficou faltando:


 - Não foi possível fazer o programa em forma de endpoints, como proposto.

 - Não foi possível usar o pagar.me para fazer o checkout do carrinho de compras.

 - Não foi possível criar uma interface gráfica para mostrar as imagens dos produtos.


Ao longo do código, em lugares onde o desenvolvedor percebeu que a solução não era a ideal porém ele não conseguiu desenvolver uma melhor solução, está anotado nos comentários como que ele sugere o melhoramento da solução.

# Obrigado pela Atenção!
# André Fernandes Hachich