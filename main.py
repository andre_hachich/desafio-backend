'''
Created on 11 de fev de 2018

@author: André Hachich
'''

from src.ProductModule import *
from src.ShoppingCartModule import *
from src.FileManagementModule import *


productCatalog = []
shoppingCartCatalog = []
'''
Read data file and fill product catalog list and shopping cart catalog list.
'''
try:
    ReadDataFiles(productCatalog, shoppingCartCatalog)
except Exception as err:
    print("Something has gone wrong:\n" + format(err))

'''
Main Loop:
Print main menu, wait for user input, and process that input,
'''
while (True):
    userinput = []
    productName = []
    productDescription = []
    productImage = []
    productFactor = []
    username = []
    productQuantity = []
    print("\n\nMAIN MENU: choose a number from the options below")
    print("1) Admin function: Add product to catalog")
    print("2) Admin function: Remove product from catalog")
    print("3) Admin function: Check current product catalog")
    print("4) Admin function: Check all shopping carts")
    print("5) User function : Add products to your shopping cart")
    print("6) User function : Remove products from your shopping cart")
    print("7) User function : Check your current shopping cart")
    print("8) User function : Pay and checkout your shopping cart")
    try:
        userinput = int(input("Enter a number: "))
        
 
        if(userinput == 1):
            '''
            Case 1: collect user input of all data related to the product (name, description, image, value and factor.
            '''
            productName = input("Enter the name of the product you would like to add: ")
            productDescription = input("Enter the description of the product you would like to add: ")
            productImage = input("Enter the image filename of the product you would like to add: ")
            productValue = int(input("Enter the value of the product you would like to add: "))
            try:
                productFactor = factor_dictionary[input("Enter the factor of the product you would like to add: ")]
            except Exception as err:
                raise Exception ("Invalid product factor (value entered: " + format(err) + "). Please enter A, B or C.")
            '''
            Add product to catalog and save file.
            '''
            AddProductToCatalog(productName, productDescription, productImage, productValue, productFactor, productCatalog)
            SaveDataFiles(productCatalog, shoppingCartCatalog)
            

        elif(userinput == 2):
            '''
            Case 2: collect user input of product name to be removed.
            '''
            productName = input("Enter the name of the product you would like to remove: ")
            '''
            Remove product from catalog and save file.
            '''
            RemoveProductFromCatalog(productName, productCatalog, shoppingCartCatalog)
            SaveDataFiles(productCatalog, shoppingCartCatalog)

        elif(userinput == 3):
            '''
            Case 3: print all products in catalog.
            '''
            print("\n\n")
            CheckProductCatalog(productCatalog)

        elif(userinput == 4):
            '''
            Case 4: print all shopping carts and its contents
            '''
            print("\n\n")
            CheckShoppingCart("", shoppingCartCatalog)


        elif(userinput == 5):
            '''
            Case 5: enter username, product and quantity, and add product to shopping cart (if 
            user already has a shopping cart) or create a new shopping cart with the product 
            (if user doesn't have a shopping cart). 
            '''
            username = input("Enter your username: ")
            productName = input("Enter the name of the product you would like to add: ")
            productQuantity = int(input("Enter the quantity you would like to add: "))
            '''
            Add product to shopping cart and save file.
            '''
            AddProductToShoppingCart(username, productName, productQuantity, shoppingCartCatalog, productCatalog)
            SaveDataFiles(productCatalog, shoppingCartCatalog)


        elif(userinput == 6):
            '''
            Case 6: enter username, product and quantity and remove from shopping cart.
            '''
            username = input("Enter your username: ")
            productName = input("Enter the name of the product you would like to remove: ")
            productQuantity = int(input("Enter the quantity you would like to remove: "))
            '''
            Remove product from shopping cart and save file.
            '''
            RemoveProductFromShoppingCart(username, productName, productQuantity, shoppingCartCatalog, productCatalog)
            SaveDataFiles(productCatalog, shoppingCartCatalog)
                
        elif(userinput == 7):
            '''
            Case 7: enter username and show shopping cart.
            '''
            username = input("Enter your username: ")
            CheckShoppingCart(username, shoppingCartCatalog)
            
        elif(userinput == 8):
            '''
            Case 8: enter username and pay and checkout you shopping cart.
            '''
            username = input("Enter your username: ") 
            '''
            Remove shopping cart from catalog and save file.
            '''
            PayAndCheckoutShoppingCart(username, shoppingCartCatalog)
            SaveDataFiles(productCatalog, shoppingCartCatalog)

    except Exception as err:
        print("Something has gone wrong:\n" + format(err))



