'''
Created on 11 de fev de 2018

@author: André Hachich
'''

from src.ProductModule import *
from src.ShoppingCartModule import *

'''
Public File Management Functions
--------------------------------
'''
def ReadDataFiles(productCatalog, shoppingCartCatalog):
    '''
    Read Data Files Method:
    -----------------------
    :param productCatalog     : empty product catalog list.
    :param shoppingCartCatalog: empty shopping cart catalog list.
    :returns                  : nothing, updates lists.
    '''
    #Open file with all products
    with open('./data/products.txt') as f:
        lines = f.readlines()
    #For every line, split string on commas and add product to catalog.
    for i in range(len(lines)):
        splitted_strings = lines[i].strip().split(',')
        AddProductToCatalog(splitted_strings[0], splitted_strings[1], splitted_strings[2], float(splitted_strings[3]), factor_dictionary[splitted_strings[4]], productCatalog)
    
    #Open file with all shopping carts
    with open('./data/shoppingcarts.txt') as f:
        lines = f.readlines()
    #For every line, split string on commas and add products to user shopping cart.
    for i in range(len(lines)):
        splitted_strings = lines[i].strip().split(',')
        j = 1
        while j < (len(splitted_strings)-1):
            AddProductToShoppingCart(splitted_strings[0], splitted_strings[j], int(splitted_strings[j+1]), shoppingCartCatalog, productCatalog)
            j += 2


def SaveDataFiles(productCatalog, shoppingCartCatalog):
    '''
    Save Data Files Method:
    -----------------------
    :param productCatalog     : filled product catalog list.
    :param shoppingCartCatalog: filled shopping cart catalog list.
    :returns                  : nothing, overwrites files with current lists.
    '''
    #Open and overwrite file with all products.
    f = open("./data/products.txt", "w")
    #For every product in the catalog, writes the product's attributes to the file.
    for i in range(len(productCatalog)):
        f.write('{:s},{:s},{:s},{:.2f},{:s}\n'.format(productCatalog[i].name, productCatalog[i].description, productCatalog[i].image, productCatalog[i].value, productCatalog[i].factor))
    f.close()
    
    #Open and overwrite file with all shopping carts
    f = open("./data/shoppingcarts.txt", "w")
    #For every shopping cart in the catalog, writes the user's name, the product and the quantity of the product to the file.
    for i in range(len(shoppingCartCatalog)):
        f.write(shoppingCartCatalog[i].name)
        for j in range(len(shoppingCartCatalog[i].prodList)):
            f.write("," + shoppingCartCatalog[i].prodList[j].name + "," + str(shoppingCartCatalog[i].prodQuantity[j]))
        f.write("\n")
    f.close()
    
    