'''
Created on 11 de fev de 2018

@author: André Hachich
'''
from enum import Enum
class Factor(Enum):
    A = 1
    B = 2
    C = 3
    '''
    Factor Enum:
    ------------
    Enumeration of all possible factors (A, B or C).
    '''
'''
Factor Dictionary:
Dictionary to allow conversion between a string input by the user and the factor Enum structure.
'''
factor_dictionary = {'A': Factor.A, 'B': Factor.B, 'C': Factor.C, 'Factor.A': Factor.A, 'Factor.B': Factor.B, 'Factor.C': Factor.C}


class Product:
    '''
    Product Class:
    --------------
    
    Attributes:
    name            - name of the product
    description     - description of the product
    image           - path to the image file of the product
    value           - product unit value
    factor          - product's factor (A, B or C)
    
    Accessibility:
    --------------
    Class object and methods are all public to all files.
 
    Developers comments:
    -------------------    
    On the accessibility:
    Ideally, this object should only be accessed and changed by the functions on this file,
    but I didn't have the time to make it work with good accessibility policy, so everything
    is currently public.
    '''
    
    
    def __init__(self, name = "", description = "", image = "", value = 0, factor = Factor.A):
        '''
        Constructor:
        ------------
        :param name       : name of the product.
        :param description: description of the product
        :param image      : path to the image file of the product
        :param value      : product unit value
        :param factor     : product's factor (A, B or C)
        :return           : nothing
        '''
        self.name = name
        self.description = description
        self.image = image
        self.value = value
        self.factor = factor

    def __str__(self):
        '''
        Print format:
        -------------
        Provides a way to print the product's attributes.
        '''
        return ('{:20s} {:30s} {:24s} ${:8.2f}   {:10s}'.format(self.name, self.description, self.image, self.value, self.factor))

def FindProductInCatalog(productName, productCatalog):
    '''
    Find Product In Catalog Method:
    -------------------------------
    :param productName    : name of the product to be found on catalog.
    :param productCatalog : all products currently listed.
    :return productIndex  : index of the product on catalog
                            -1 if not found.
    '''
    productIndex = -1
    for i in range(len(productCatalog)):
        #This loop finds the object Product based on the name string
        if (productCatalog[i].name.lower() == productName.lower()):
            productIndex = i
    return productIndex



'''
Public Product Functions
-------------------------


Developers comments:
--------------------
Ideally only the functions hereafter should be public to other files.
'''    

def AddProductToCatalog(productName, productDescription, productImage, productValue, productFactor, productCatalog):
    '''
    Add Product To Catalog Method:
    ------------------------------
    :param productName       : name of the product to be added
    :param productDescription: description of the product to be added
    :param productImage      : path to image file of the product to be added
    :param productValue      : unit value of the product to be added
    :param productFactor     : factor of the product to be added
    :param productCatalog    : all products currently listed
    :return                  : nothing, update product catalog.
    :exception               : if user tries to add an already existing product to product catalog.
    '''
    #Check if product already exists
    if (FindProductInCatalog(productName, productCatalog) == -1):
        productCatalog.append(Product(productName, productDescription, productImage, productValue, productFactor))
    else:
        raise Exception("Error: tried to add existing product (" + productName + ") to catalog")
        
def RemoveProductFromCatalog(productName, productCatalog, shoppingCartCatalog):
    '''
    Remove Product From Catalog Method:
    -----------------------------------
    :param productName         : name of the product to be removed
    :param productCatalog      : all products currently listed
    :param shoppingCartCatalog : all shopping carts currently listed
    :return                    : nothing, update catalogs
    :exception                 : if user tries to remove an already unavailable product from catalog.
    '''
    #Check if product exists
    productIndex = FindProductInCatalog(productName, productCatalog)
    if (productIndex == -1):
        raise Exception("Error: tried to remove an unavailable product (" + productName + ") from catalog")
    else:
        #If a product was removed, remove it from all existing shopping carts
        for i in range(len(shoppingCartCatalog)):
            for j in range(len(shoppingCartCatalog[i].prodList)):
                if (shoppingCartCatalog[i].prodList[j] == productCatalog[productIndex]):
                    shoppingCartCatalog[i].RemoveFromCart(productCatalog[productIndex], shoppingCartCatalog[i].prodQuantity[j])
                    break
        #Remove product from catalog
        productCatalog.pop(productIndex)
        
def CheckProductCatalog(productCatalog):
    '''
    Check Product Catalog Method:
    -----------------------------
    :param productCatalog : all shopping carts currently listed
    :return               : nothing, prints all products.
    '''
    print('{:20s} {:30s} {:24s} {:10s}  {:10s}'.format("NAME", "DESCRIPTION", "IMAGE FILE", "VALUE", "FACTOR"))
    for i in range(len(productCatalog)):
        print(productCatalog[i])





        
