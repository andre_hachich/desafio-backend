'''
Created on 11 de fev de 2018

@author: André Hachich
'''
from src.ProductModule import *

class ShoppingCart:
    '''
    ShoppingCart Class:
    -------------------
    
    Attributes:
    name            - name of the user who owns the shopping cart
    prodList        - list of products that are on the shopping cart
    prodQuantity    - list of quantity of each product on the shopping cart 
    prodDiscount    - list of discounts of each product on the shopping cart
    prodTotalValue  - list of total value based on unit value and discounts.
    cartValue       - number containing the value of all products contained on the shopping cart.
    cartDiscount    - number containing overall discount applied to the shopping cart.
    
    Accessibility:
    --------------
    Class object and methods are all public to all files.
 
    Developers comments:
    -------------------
    On the many lists:
    I considered making another class to encapsulate the product with the quantity, discount
    and total value, instead of making four separate lists. As there was already a "product"
    class, I figured it would be confusing to have more than one class of products, one for
    the product catalog and one for products in the shopping cart. I also considered only
    using one class of products and whenever I wanted to show them on the catalog, omit the
    "quantity", "discount" and "total value" attributes, but I considered this solution to
    be better.
    
    On the accessibility:
    Ideally, this object should only be accessed and changed by the functions on this file,
    but I didn't have the time to make it work with good accessibility policy, so everything
    is currently public.
    '''
    

    def __init__(self, name = ""):
        '''
        Constructor:
        ------------
        :param name: the user name of the shopping cart owner.
        :return    : nothing, initializes lists.
        '''
        self.name = name
        self.prodList = []
        self.prodQuantity = []
        self.prodDiscount = []
        self.prodTotalValue = []
        self.cartValue = 0
        self.cartDiscount = 0


    def AddToCart(self, product, quantity):
        '''
        Add to Cart Method:
        -------------------
        :param product : the product object already initialized.
        :param quantity: the quantity of products to be added.
        :return        : nothing, adds product to lists and computes cart value.
        '''    
        #Check if user is adding a new item or just adding more of an already existing item
        if (self.prodList.count(product) == 0):
            #If it is a new item, add them to the lists
            self.prodList.append(product)
            self.prodQuantity.append(quantity)
            #Add temporary values to the list, to be updated in the method ComputeDiscountAndTotal
            self.prodDiscount.append([0])
            self.prodTotalValue.append([0])
        else:
            #If it is an existing item, update quantity
            self.prodQuantity[self.prodList.index(product)] += quantity
        #Recalculate the discount after the new addition
        self.ComputeDiscountAndTotal()
    
    def RemoveFromCart(self, product, quantity):
        '''
        Remove from Cart Method:
        ------------------------
        :param product : the product object already initialized.
        :param quantity: the quantity of products to be removed.
        :return        : nothing, removes product from lists and computes cart value.
        :exception     : if the user tries to remove a product that is not on his current shopping cart.
        '''
        #Check if user is removing a product that is on cart
        if (self.prodList.count(product) == 0):
            #If not, send an error message
            raise Exception("Error: There is no " + product.name + " on the shopping cart")
        else:
            #If the product is on the cart, find its index
            indexToRemove = self.prodList.index(product)
            #If user removed a quantity greater or equal to the quantity on the cart, remove it from list
            if (quantity >= self.prodQuantity[indexToRemove]):
                self.prodList.pop(indexToRemove)
                self.prodQuantity.pop(indexToRemove)
                self.prodDiscount.pop(indexToRemove)
                self.prodTotalValue.pop(indexToRemove)
            else:
                self.prodQuantity[indexToRemove] -= quantity
        #Recalculate the discount after the removal
        self.ComputeDiscountAndTotal()
        
    def CheckCart(self):
        '''
        Check Cart Method:
        ------------------
        :return: nothing, prints all attributes from cart.
        '''
        print("\nSHOPPING CART FROM " + self.name)
        print('{:20s} {:30s} {:24s} {:10s}  {:10s} {:10s} {:10s}  {:12s}'.format("NAME", "DESCRIPTION", "IMAGE FILE", "VALUE", "FACTOR", "QUANTITY", "DISCOUNT", "TOTAL VALUE"))
        for i in range(len(self.prodList)):
            print(self.prodList[i], end='')
            print('{:10.0f} {:9.0f}%   ${:10.2f}'.format(self.prodQuantity[i], (self.prodDiscount[i]*100), self.prodTotalValue[i]))
        print("\n\nCART VALUE:\t\t$" + str('%.2f' % self.cartValue))
        print("CART DISCOUNT:\t\t" + str('%.1f' % ((self.cartDiscount)*100)) + "%")
        
        
        
    def ComputeDiscountAndTotal(self):
        '''
        Compute Discount and Total Method:
        ----------------------------------
        :return: nothing, computes all values and discounts based on
                 the factor and quantity of products.
        '''
        factorAQuantity = 0
        factorBQuantity = 0
        factorCQuantity = 0
        cartOriginalValue = 0
        
        #Count the quantity of products of each factor
        for i in range(len(self.prodList)):
            if (Factor.A == self.prodList[i].factor):
                factorAQuantity += self.prodQuantity[i]
            elif (Factor.B == self.prodList[i].factor):
                factorBQuantity += self.prodQuantity[i]
            elif (Factor.C == self.prodList[i].factor):
                factorCQuantity += self.prodQuantity[i]
        
        #Compute the discount in each factor
        factorADiscount = min(factorAQuantity * 0.01, 0.05)
        factorBDiscount = min(factorBQuantity * 0.05, 0.15)
        factorCDiscount = min(factorCQuantity * 0.10, 0.30)
        
        #Update the discount and the total value for each product
        for i in range(len(self.prodList)):
            #Store the original cart value (without all the discounts) to compute overall discount
            cartOriginalValue += self.prodQuantity[i] * self.prodList[i].value
            #Compute the product value after discount
            if (Factor.A == self.prodList[i].factor):
                self.prodDiscount[i] = factorADiscount
                self.prodTotalValue[i] = self.prodQuantity[i] * self.prodList[i].value * (1 - self.prodDiscount[i])
            elif(Factor.B == self.prodList[i].factor):
                self.prodDiscount[i] = factorBDiscount
                self.prodTotalValue[i] = self.prodQuantity[i] * self.prodList[i].value * (1 - self.prodDiscount[i])
            elif(Factor.C == self.prodList[i].factor):
                self.prodDiscount[i] = factorCDiscount
                self.prodTotalValue[i] = self.prodQuantity[i] * self.prodList[i].value * (1 - self.prodDiscount[i])
        
        #Compute cart total value after discounts
        self.cartValue = sum(self.prodTotalValue)
        #If cart original value is not zero, compute overall discount.
        if (cartOriginalValue > 0):
            self.cartDiscount = 1 - (self.cartValue / cartOriginalValue)
        else:
            self.cartDiscount = 0
            
        #Double check if overall discount is less than 30%
        #Mathematically, it should never enter this condition, since no product on cart has more than 30% discount.
        if (self.cartDiscount > 0.30):
            self.cartDiscount = 0.30
            self.cartValue = self.cartDiscount * cartOriginalValue
            
def FindShoppingCartInCatalog(username, shoppingCartCatalog):
    '''
    Find Shopping Cart In Catalog Method:
    -------------------------------------
    :param username           : name of the user to find shopping cart.
    :param shoppingCartCatalog: all shopping carts currently listed.
    :return shoppingCartIndex : index of the user's shopping cart on catalog
                                -1 if not found.
    '''
    shoppingCartIndex = -1
    for i in range(len(shoppingCartCatalog)):
        #This loop finds the object Product based on the name string
        if (shoppingCartCatalog[i].name.lower() == username.lower()):
            shoppingCartIndex = i
    return shoppingCartIndex
       
            
'''
Public Shopping Cart Functions
-------------------------------


Developers comments:
--------------------
Ideally only the functions hereafter should be public to other files.
'''        
def AddProductToShoppingCart(username, productName, productQuantity, shoppingCartCatalog, productCatalog):
    '''
    Add Product To Shopping Cart Method:
    ------------------------------------
    :param username            : name of the user to find shopping cart
    :param productName         : name of the product to be added
    :param productQuantity     : quantity of the product to be added
    :param shoppingCartCatalog : all shopping carts currently listed
    :param productCatalog      : all products currently listed
    :return                    : nothing, update catalogs
    :exception                 : if user tries to add non-existing product to cart.
    '''

    #Find user's shopping cart index
    shoppingCartIndex = FindShoppingCartInCatalog(username, shoppingCartCatalog)
    #If the user is new, create a shopping cart for the user.
    if (shoppingCartIndex == -1):
        shoppingCartCatalog.append(ShoppingCart(username))
    
    #Check if product exists.
    productIndex = FindProductInCatalog(productName, productCatalog)
    if (productIndex != -1):
        #Add product to the last element of shopping Cart Catalog
        shoppingCartCatalog[shoppingCartIndex].AddToCart(productCatalog[productIndex], productQuantity)
    else:
        raise Exception("Error: Costumer tried to add " + productName + ", an unavailable product, to the cart")

def RemoveProductFromShoppingCart(username, productName, productQuantity, shoppingCartCatalog, productCatalog):
    '''
    Remove Product From Shopping Cart Method:
    -----------------------------------------
    :param username            : name of the user to find shopping cart
    :param productName         : name of the product to be removed
    :param productQuantity     : quantity of the product to be removed
    :param shoppingCartCatalog : all shopping carts currently listed
    :param productCatalog      : all products currently listed
    :return                    : nothing, update catalogs
    :exception                 : if user tries to remove non-existing product to cart.
    :exception                 : if user doesn't have a shopping cart
    '''
    
    #Check if user exists
    userIndex = FindShoppingCartInCatalog(username, shoppingCartCatalog)
    if (userIndex != -1):
        productIndex = FindProductInCatalog(productName, productCatalog)
        #Check if product was found.
        if (productIndex != -1):
            shoppingCartCatalog[userIndex].RemoveFromCart(productCatalog[productIndex], productQuantity)
            #If there is no products left on the shopping cart, remove shopping cart.
            if (len(shoppingCartCatalog[userIndex].prodList) == 0):
                shoppingCartCatalog.pop(userIndex)
                print("Costumer " + username + " has emptied his shopping cart.")
        else:
            raise Exception("Error: Costumer tried to remove " + productName + ", an unavailable product, from the cart")
    else:
        raise Exception("Error: costumer doesn't have a shopping cart.")
    
        
def PayAndCheckoutShoppingCart(username, shoppingCartCatalog):
    '''
    Pay And Checkout Shopping Cart Method:
    --------------------------------------
    :param username            : name of the user to find shopping cart
    :param shoppingCartCatalog : all shopping carts currently listed
    :return                    : nothing, update catalogs
    :exception                 : if user doesn't have a shopping cart
    '''
    #Check if user exists.
    userIndex = FindShoppingCartInCatalog(username, shoppingCartCatalog)
    if (userIndex != -1):
        #Send a message to inform the payment and remove user's shopping cart from catalog.
        print("Costumer " + username + " payed a total of $" + str('%.2f' % shoppingCartCatalog[userIndex].cartValue) + " and checked out his shopping cart")
        shoppingCartCatalog.pop(userIndex)
    else:
        raise Exception("Error: costumer " + username + " does not have a shopping cart registered")
    
def CheckShoppingCart(username, shoppingCartCatalog):
    '''
    Pay And Checkout Shopping Cart Method:
    --------------------------------------
    :param username            : name of the user to find shopping cart. If no name is informed,
                                 all shopping carts will be printed.
    :param shoppingCartCatalog : all shopping carts currently listed
    :return                    : nothing, update catalogs
    '''
    #If a username was found
    userIndex = FindShoppingCartInCatalog(username, shoppingCartCatalog)
    if (userIndex != -1):
        #Print user's shopping cart
        shoppingCartCatalog[userIndex].CheckCart()
    else:
        #Print all shopping carts
        for i in range(len(shoppingCartCatalog)):
            shoppingCartCatalog[i].CheckCart()



    